﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum EElement 
{
    WATER,
    EARTH,
    AIR,
    FIRE,
    THUNDER
}

public static class EElementExtentios
{
    private static Dictionary<EElement, List<EElement>> s_AdvantageDictionary = null;

    static EElementExtentios()
    {
        s_AdvantageDictionary = new Dictionary<EElement, List<EElement>>();

        s_AdvantageDictionary.Add(EElement.WATER, new List<EElement>() { });
        s_AdvantageDictionary.Add(EElement.EARTH, new List<EElement>() { });
        s_AdvantageDictionary.Add(EElement.AIR, new List<EElement>() { });
        s_AdvantageDictionary.Add(EElement.FIRE, new List<EElement>() { });
        s_AdvantageDictionary.Add(EElement.THUNDER, new List<EElement>() { });
    }

    public static bool HasAdvantage(this EElement thisElement, EElement other)
    {
        return s_AdvantageDictionary[thisElement].Contains(other);
    }
}
