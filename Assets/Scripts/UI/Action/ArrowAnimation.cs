﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//TODO: remove this class and use Animation

public class ArrowAnimation : MonoBehaviour {
    private Vector2 m_Velocity;
    [SerializeField]
    private Vector2 m_Acceleration;

    private float m_AnimationStep;
    [SerializeField]
    private float m_AnimationTime;
    private bool m_PlayingAnimation;

    private Vector3 m_InitialPosition;

    private Image m_ArrowImage;

    void Awake()
    {
        m_ArrowImage = GetComponent<Image>();
    }

    void Start()
    {
        m_ArrowImage.color = new Color(1, 1, 1, 0);

        m_InitialPosition = transform.position;
    }

    void Update()
    {
        
        if(m_PlayingAnimation)
        {
            float oldAnimationStep = m_AnimationStep;
            m_AnimationStep += Time.deltaTime;
            float step = 0;

            bool endAnimation = false;

            //completly stop
            if(m_AnimationStep >= m_AnimationTime)
            {
                endAnimation = true;
                m_AnimationStep = m_AnimationTime;
            }

            step = m_AnimationStep - oldAnimationStep;

            
            if(m_AnimationStep >= GetBreakTime())
            {
                //move with acceleration all remaining time before breaking
                if(oldAnimationStep < GetBreakTime())
                {
                    m_Velocity += (m_Acceleration * (GetBreakTime() - oldAnimationStep));
                    m_Velocity -= (m_Acceleration * (m_AnimationStep - GetBreakTime()));
                }
                //break
                else
                {
                    m_Velocity -= (m_Acceleration * step);
                }
            }
            //accelerate
            else
            {
                m_Velocity += (step * m_Acceleration);
            }

            //change position
            ((RectTransform)transform).Translate(m_Velocity * step);
            //change alpha
            m_ArrowImage.color = Color.Lerp(new Color(1, 1, 1, 0), new Color(1, 1, 1, 1), m_AnimationStep / m_AnimationTime);

            if (endAnimation)
            {
                ResetAnimation();
            }
        }
    }

    public void StartAnimation()
    {
        m_PlayingAnimation = true;
    }

    public void StopAnimation()
    {
        m_PlayingAnimation = false;
        ResetAnimation();
    }

    private void ResetAnimation()
    {
        m_Velocity = Vector2.zero;
        m_AnimationStep = 0;
        transform.position = m_InitialPosition;
        m_ArrowImage.color = new Color(1, 1, 1, 0);
    }

    private float GetBreakTime()
    {
        return m_AnimationTime / 2f;
    }
}
