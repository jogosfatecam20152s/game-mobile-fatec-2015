﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class OrbButtonsPanel : MonoBehaviour {

    [SerializeField]
    private Orb[] m_Orbs = new Orb[3];

    private List<Orb> m_Sequence;

	void Start () 
    {
        m_Sequence = new List<Orb>(m_Orbs.Length);
	}
	

	void Update () 
    {

	}

    public bool IsSpellSelected()
    {
        return m_Sequence.Count >= m_Orbs.Length;
    }

    public void SelectOrb(Orb orb)
    {
        if(IsSpellSelected())
        {
            throw new Exception("Spell has already been selected.");
        }

        m_Sequence.Add(orb);

        //cehck wether the spell has been selected
        if(IsSpellSelected())
        {
            //calculates the combination id
            var combinationId = m_Sequence.Sum(x => (int)x.m_OrbType);

            //informs the player manager which is the selected spell
            var player = GameObject.Find("Player");
            var playerController = player.GetComponent<PlayerController>();
            playerController.SetSelectedSpell(combinationId);
        }
    }

    public void Refresh()
    {
        m_Sequence.Clear();

        foreach(var orb in m_Orbs)
        {
            orb.Refresh();
        }
    }
}
