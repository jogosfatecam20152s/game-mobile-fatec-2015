﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class OrbButton : MonoBehaviour {

    public float m_Radious = 50;

	void Start () 
    {
        GetComponent<Button>().onClick.AddListener(CheckArea);
	}
	

	void Update ()
    {

	}

    /*
     *  Calculates whether input point is inside the circular touch area 
     */
    private void CheckArea()
    {
        Vector2 position2D = new Vector2(transform.position.x, transform.position.y);
        Vector2 mousePosition2D = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        if((mousePosition2D - position2D).magnitude <= m_Radious)
        {
            OnClick();
        }
    }

    private void OnClick()
    {
        GetComponentInParent<Orb>().SelectOrb();
    }

}
