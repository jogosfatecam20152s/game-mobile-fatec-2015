﻿using UnityEngine;
using System.Collections;

public class Orb : MonoBehaviour {

    [SerializeField]
    public EOrbType m_OrbType;

    private SelectionBar m_SelectionBar;

    void Awake()
    {
        m_SelectionBar = GetComponentInChildren<SelectionBar>();
    }

    public void SelectOrb()
    {
        var buttonsPanel = GetComponentInParent<OrbButtonsPanel>();

        if(buttonsPanel.IsSpellSelected())
        {
            //There is already an active spell
        }
        else
        {
            m_SelectionBar.OnOrbSelected();

            buttonsPanel.SelectOrb(this);
        }
    }

    public void Refresh()
    {
        m_SelectionBar.ClearSelection();
    }
}
