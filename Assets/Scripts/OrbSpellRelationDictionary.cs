﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class OrbSpellRelationDictionary : MonoBehaviour 
{
    /*
     * UNITY DOES NOT HAVE SUPPORT FOR DICTIONARY SERIALIZATION
     */

    public static readonly int MAX_COMBINATIONS = 10;//Pra calcular é só usar a formula de combinação com repetição

    public OrbSpellRelation[] m_Relations = new OrbSpellRelation[MAX_COMBINATIONS];

    private Dictionary<int, Spell> m_RelationDictionary;

    public OrbSpellRelationDictionary()
    {
        int i = 0;
        for (int a = 0; a < 3; a++)
        {
            for (int b = a; b < 3; b++)
            {
                for (int c = b; c < 3; c++)
                {
                    m_Relations[i] = new OrbSpellRelation()
                    {
                        m_TypeOne = (EOrbType)Enum.GetValues(typeof(EOrbType)).GetValue(a),
                        m_TypeTwo = (EOrbType)Enum.GetValues(typeof(EOrbType)).GetValue(b),
                        m_TypeThree = (EOrbType)Enum.GetValues(typeof(EOrbType)).GetValue(c)
                    };

                    i++;
                }
            }
        }
    }

    void Awake()
    {
        //Initialize and populate dictionary
        m_RelationDictionary = new Dictionary<int, Spell>();
        foreach(var relation in m_Relations)
        {
            m_RelationDictionary.Add(relation.CombinationId(), relation.m_Spell);
        }

    }

    public Spell GetSpell(int combinationId)
    {
        return m_RelationDictionary[combinationId];
    }
}
