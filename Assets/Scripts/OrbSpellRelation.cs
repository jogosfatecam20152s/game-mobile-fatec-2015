﻿using System;

[Serializable]
public class OrbSpellRelation{

    public EOrbType m_TypeOne, m_TypeTwo, m_TypeThree;
    public Spell m_Spell;

    public int CombinationId()
    {
        return ((int)m_TypeOne) + ((int)m_TypeTwo) + ((int)m_TypeThree);
    }
}
