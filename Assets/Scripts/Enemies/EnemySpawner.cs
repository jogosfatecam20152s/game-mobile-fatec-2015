﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour 
{
    [SerializeField]
    private GameObjectPool m_EnemyPool;

    [SerializeField]
    public float m_MinLeft;
    [SerializeField]
    public float m_MaxRight;

    public void Spawn(int quantity, EElement type)
    {
        for(int i=0; i<quantity; i++)
        {
            var enemy = m_EnemyPool.NextObject();

            enemy.transform.position = new Vector3(Random.Range(m_MinLeft, m_MaxRight), transform.position.y, enemy.transform.position.z);
            enemy.GetComponent<Enemy>().m_Element = type;

            enemy.SetActive(true);
        }
    }

    public void Update()
    {
        if(Input.GetKeyDown("k"))
        {
            Spawn(4, EElement.FIRE);
        }
    }
}
