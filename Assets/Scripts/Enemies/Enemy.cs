﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
    [SerializeField]
    [Range(0, 1)]
    private float m_Speed;

    [SerializeField]
    private float m_Damage = 5;

    public EElement m_Element;

    private Animator m_Animator;
    private static readonly float MAX_SPEED = 1f;

    private GameObject m_EndPoint;
    private Player m_Player;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Animator.speed = (m_Speed / MAX_SPEED) * 2.4f;

        m_EndPoint = GameObject.Find("EndPoint");
        m_Player = GameObject.Find("Player").GetComponent<Player>();
    }

    void Update()
    {
        if(/*!isDead*/true)
        {
            Vector3 step = m_Speed * new Vector3(0, -1, 0) * Time.deltaTime;
            transform.Translate(step);
        }

        //Check if enemy reached the goal and apply the damage to the player
        if(transform.position.y < m_EndPoint.transform.position.y)
        {
            m_Player.TakeDamage(m_Damage);

            OnVanish();
        }
    }

    public void Kill()
    {
        //TODO: Play death animation
    }

    private void OnVanish()
    {
        gameObject.SetActive(false);
    }
}
