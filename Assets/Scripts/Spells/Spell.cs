﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Spell : MonoBehaviour {

    [SerializeField]
    private SpellIcon m_UIIcon;

    [SerializeField]
    private EElement m_Element;

    public void SetAsCurrentSpell()
    {
        m_UIIcon.ActivateSpell();
    }

    public void UnselectSpell()
    {
        m_UIIcon.DeactivateSpell();
    }

    public bool CastSpell()
    {
        if (m_UIIcon.CastSpell())
        {
            var enemies = GameObject.FindGameObjectsWithTag("Enemy").Select(x => x.GetComponent<Enemy>());

            foreach(var enemy in enemies)
            {
                if(m_Element.HasAdvantage(enemy.m_Element))
                {
                    enemy.Kill();
                }
            }

            return true;
        }

        return false;
    }
}
