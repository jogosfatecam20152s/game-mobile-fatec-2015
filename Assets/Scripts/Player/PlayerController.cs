﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {

    private OrbSpellRelationDictionary m_OrbSpellRelationDictionary;

    private Spell m_SelectedSpell = null;

    void Awake()
    {
        m_OrbSpellRelationDictionary = GetComponent<OrbSpellRelationDictionary>();
    }

    public void SetSelectedSpell(int combinationId)
    {
        m_SelectedSpell = m_OrbSpellRelationDictionary.GetSpell(combinationId);
        m_SelectedSpell.SetAsCurrentSpell();
    }

    public void CancelSpell()
    {
        if (HasSelectedSpell())
        {
            m_SelectedSpell.UnselectSpell();
            m_SelectedSpell = null;
        }

        //Reset any selected orb in the buttons panel
        GameObject.Find("ButtonsPanel").GetComponent<OrbButtonsPanel>().Refresh();
    }

    /*
     * Returns wether the Player Controller was able to cast the spell or not.
     */
    public bool CastSpell()
    {
        if(HasSelectedSpell())
        {
            return m_SelectedSpell.CastSpell();
        }

        return false;
    }

    public bool HasSelectedSpell()
    {
        return m_SelectedSpell;
    }
}
