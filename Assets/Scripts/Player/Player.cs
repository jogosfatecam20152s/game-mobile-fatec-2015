﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    [SerializeField]
    private float m_Health = 100;

    [SerializeField]
    private float m_Score = 0;

    public void TakeDamage(float quantity)
    {
        m_Health -= quantity;
        //update and play UI animation

        CheckDeath();
    }

    public void GainPoints(float quantity)
    {
        m_Score += quantity;
        //update and play UI animation
    }

    public void CheckDeath()
    {
        if(m_Health <= 0)
        {
            OnDeath();
        }
    }

    private void OnDeath()
    {

    }
}
