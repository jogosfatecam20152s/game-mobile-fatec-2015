﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameObjectPool : MonoBehaviour
{

    [SerializeField]
    private GameObject m_ObjectPrefab;

    [SerializeField]
    private int m_InitialCapacity = 10;

    [SerializeField]
    private bool m_Static = false;

    private List<GameObject> m_Pool;

    public GameObject Prefab { get { return m_ObjectPrefab; } }

    void Start()
    {
        m_Pool = new List<GameObject>(m_InitialCapacity);
        for (int i = 0; i < m_InitialCapacity; i++)
            CreateNewObject();
    }

    private GameObject CreateNewObject()
    {
        GameObject go = Instantiate<GameObject>(m_ObjectPrefab);
        go.SetActive(false);
        m_Pool.Add(go);
        return go;
    }

    public GameObject NextObject()
    {
        foreach (GameObject go in m_Pool)
            if (!go.activeInHierarchy)
                return go;

        if (!m_Static)
            return CreateNewObject();

        return null;
    }
}
